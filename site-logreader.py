import datetime
from db import *
from pprint import pprint
import json
from flask import Flask
#app = Flask(__name__)
prefix = '/'

@app.route('/')
def display_logs(limit=250):
	l = int(limit)
	output = {}
	data = []
	m = Log.query.order_by("log_date desc").limit(l)
	
	for r in m:
		piece = []
		piece.append(str(r.log_date))
		piece.append(str(r.source_ip))
		piece.append(str(r.source_port))
		piece.append(str(r.destination_ip))
		piece.append(str(r.destination_port))
		piece.append(str(r.type))
		piece.append(str(r.len))
		piece.append(str(r.message))
		data.append(piece)
	output['data']=data
	# read it out now
	db.session.commit()
	return Response(json.dumps(output, sort_keys=True, indent=4),'text/json')
#app.url_map.strict_slashes = False
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.run(host='0.0.0.0',port=8888)

