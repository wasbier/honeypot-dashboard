d_map = null;
d_country = null;
d_regions = null;
d_port = null;
d_ip = null;
d_attackpie = null;
d_line = null;

google.charts.load('upcoming', {'packages':['corechart', 'line', 'bar', 'geochart']});

	function drawRegionsMap(response) {
		var data = google.visualization.arrayToDataTable(d_regions);

		var options = {
			defaultColor: '#ff0000'
		};
		var chart = new google.visualization.GeoChart(document.getElementById('world-map'));
		chart.draw(data, options);
	}

	function drawPortBarChart() {
		var data = google.visualization.arrayToDataTable(d_port);
		var options = {
	  		chart: {
			subtitle: 'Top Port connecting to honeypot hosts'
	  	},
	  	hAxis: {
			title: 'Total Connections',
			logScale: true,
			minValue: 0,
	  	},
	  	vAxis: {
			title: 'IP Addresses',
			logScale: true
		  },
	  	legend: { position: "none" },
	  	bars: 'vertical'
		};
		var material = new google.charts.Bar(document.getElementById('top-ports'));
		material.draw(data, options);
    }

	function drawIpBarChart() {
		var data = google.visualization.arrayToDataTable(d_ip);

		var options = {
	  		chart: {
			subtitle: 'Top Addresses connecting to honeypot hosts'
	 	 },
 
	  	hAxis: {
		  	logScale: true
		  },
	  yAxis: {
	  	logScale: true
	  },
	  legend: { position: "none" },
	  bars: 'horizontal'
		};
		var material = new google.charts.Bar(document.getElementById('top-talkers'));
		material.draw(data, options);
    }

   function drawCountryPieChart() {
    	var data = google.visualization.arrayToDataTable(d_country);

	  var options = { 
			chartArea: {
				left: 15,
				top: 10,
				width:'200%',
				height:'150%'
			}
	  };

	  var chart = new google.visualization.PieChart(document.getElementById('top-countries'));

	  chart.draw(data, options);
    }

    function drawAttackPieChart() {
    	var data = google.visualization.arrayToDataTable(d_attackpie);

	  var options = { 
			chartArea: {
				left: 15,
				top: 10,
				width:'200%',
				height:'150%'
			}
	  };

	  var chart = new google.visualization.PieChart(document.getElementById('attack-types'));

	  chart.draw(data, options);
    }

	function drawLineGraph() {

	var data = new google.visualization.DataTable();
	data.addColumn('datetime', 'Date Time');
	data.addColumn('number', 'Count');

	data.addRows(d_line);

	var options = {
	  chart: {
	    subtitle: 'Activity over last 24 hours'
	  },
	  legend: { position: "none" },
	  //width: 900,
	  //height: 500
	};

	var chart = new google.charts.Line(document.getElementById('traffic-activity'));

	chart.draw(data, options);
    }


$.getJSON("http://fileville.net/dynamic", function( data ) {
	api_key = "AIzaSyAzcTqi52_6aP0ExMYKhJ-j_QQASqvtftE";
        d_map = data['countries'];
        d_country = data['countries'];
	d_regions = data['regions'];
	console.log(d_country);
        d_port = data['top-ports'];
        d_ip = data['top-addresses'];
        d_attackpie = data['top-attacks'];
        d_line_raw = data['activity'];
        console.log(data);
	google.charts.setOnLoadCallback(drawIpBarChart);
	google.charts.setOnLoadCallback(drawRegionsMap);
	google.charts.setOnLoadCallback(drawPortBarChart);
	google.charts.setOnLoadCallback(drawCountryPieChart);
	google.charts.setOnLoadCallback(drawAttackPieChart);
	d_line = [];
	for (var i = 0; i < d_line_raw.length; i++) {
		date = new Date(d_line_raw[i][0]);
		count = parseInt(d_line_raw[i][1]);
		d_line.push([date,count]);
	}
	google.charts.setOnLoadCallback(drawLineGraph);
});
