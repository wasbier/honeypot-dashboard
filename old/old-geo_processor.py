from pprint import pprint
from db import *
from geoip import geolite2
import pycountry

#open_database
#geodb = "geoip.db.mmdb"
#with open_database(geodb) as geolookup:

# clear out our data
db.session.execute("delete from geo")
db.session.commit()
dd = Log.query.all()
matches={}
for record in dd:
	ip = record.source_ip
	#match = ""
	#if ip not in matches:
	match = geolite2.lookup(ip)
	#	matches[ip]=match
	#else:
	#	match = matches[ip]
	if match is not None:
		unknown = False
		country_code = str(match.country)
		try:
			unknown = False
			country_name = str(pycountry.countries.lookup(country_code).name)
			print "Added Country for ip %s: %s" % (ip,country_name)
		except:
			print "Unknown Country for ip %s: %s \t %s " % (ip,country_code,country_name)
			country_name = "Unknown"
			unknown = True
		if not unknown:
			if "," in country_name:
				country_name = country_name.split(",")[0]
			query = "insert into geo(country_code,country_name,count) VALUES (:code,:name,1) on duplicate key update count = count + 1;"
			#pprint(query)
			q = db.session.execute(query,{'code':country_code,'name':country_name})
			db.session.commit()
			#pprint(q)
print "Done... (Hopefully)"
