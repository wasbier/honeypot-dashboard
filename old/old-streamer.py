import datetime
import socket
import pycountry
from utils import Message
from pprint import pprint
from utils import Message
from geoip import geolite2
from db import *

TCP_IP = '127.0.0.1'
TCP_PORT = 5145
BUFFER_SIZE = 2048

def process(line):
		final = Message()
		messages = {
		 	'src': 'source_ip',
		 	'dst': 'destination_ip',
 			'spt': 'source_port',
 			'dpt': 'destination_port',
 			'ttl':'ttl',
 			'len': 'packet_length'
 		}
		od = str(line).lower().split()
		pprint(od)
		print("\r\r\n\r\n\r\n\n")
		# rebuild our date based on linux timestamp format
		#reconstructed_date = "%s %s 2018 %s" % (str(od[0]).title(),str(od[1]),str(od[2]))
		#date = datetime.datetime.strptime(reconstructed_date,"%b %d %Y %H:%M:%S")
		del(od[0:2])
		date=None
		# now get our data
		final.add('date',date)
		for field in od:
			of = field.split("=")
			# because sometimes its an int?????
			key = str(of[0]).split()[0]
			if key in messages.keys():
				final.add(messages[key],of[1])
		if final['destination_port'] is None:
			print "Unknown Value!"
			#pprint(final)
		else:
			l = Log(date,final['source_ip'],final['destination_ip'],final['source_port'],final['destination_port'],final['ttl'],final['len'],"fw_accept")
			lookup_geo(final['source_ip'])
			db.session.add(l)
			db.session.commit()
			print "Adding value to database %s -> %d" % (final['source_ip'],int(final['destination_port']))
		return final

def lookup_geo(record):
	ip = record.source_ip
	match = geolite2.lookup(ip)
	if match is not None:
		unknown = False
		country_code = str(match.country)
		try:
			unknown = False
			country_name = str(pycountry.countries.lookup(country_code).name)
			print "Added Country for ip %s: %s" % (ip,country_name)
		except:
			print "Unknown Country for ip %s: %s \t %s " % (ip,country_code,country_name)
			country_name = "Unknown"
			unknown = True
		if not unknown:
			if "," in country_name:
				country_name = country_name.split(",")[0]
			query = "insert into geo(country_code,country_name,count) VALUES (:code,:name,1) on duplicate key update count = count + 1;"
			#pprint(query)
			q = db.session.execute(query,{'code':country_code,'name':country_name})
			db.session.commit()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print 'Connection address:', addr
while 1:
	data = conn.recv(BUFFER_SIZE)
	#if not data: break
	pprint(data)
	process(data)
conn.close()
	
