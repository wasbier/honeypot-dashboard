from os.path import isfile
import sys
from pprint import pprint
import datetime
from utils import Message
from db import *

class iptables():
	file = None
	callback = None

	def open(self, file):
		if isfile(file):
			self.file = file
			return True
		else:
			return False
	def set(self, call):
		self.callback = call

	def __init__(self,file):
		self.open(file)
		self.set(self.process)

	def read(self):
		data = []
		if self.file is not None:
			with open(self.file) as f:
				for line in f:
					if self.callback is not None:
						self.process(line)
				f.close()
			print "Beginning final sync to database..."
			db.session.commit()
		return data

	def process(self,line):
		final = Message()
		messages = {
		 	'src': 'source_ip',
		 	'dst': 'destination_ip',
 			'spt': 'source_port',
 			'dpt': 'destination_port',
 			'ttl':'ttl',
 			'len': 'packet_length'
 		}
		od = str(line).lower().split()
		# rebuild our date based on linux timestamp format
		reconstructed_date = "%s %s 2017 %s" % (str(od[0]).title(),str(od[1]),str(od[2]))
		date = datetime.datetime.strptime(reconstructed_date,"%b %d %Y %H:%M:%S")
		del(od[0:2])

		# now get our data
		final.add('date',date)
		for field in od:
			of = field.split("=")
			# because sometimes its an int?????
			key = str(of[0]).split()[0]
			if key in messages.keys():
				final.add(messages[key],of[1])
		if final['destination_port'] is None:
			print "Unknown Value!"
			#pprint(final)
		else:
			l = Log(date,final['source_ip'],final['destination_ip'],final['source_port'],final['destination_port'],final['ttl'],final['len'],"fw_accept")
			db.session.add(l)
			db.session.commit()
			print "Adding value to database %s -> %d" % (final['source_ip'],int(final['destination_port']))
		return final
if len(sys.argv) > 1:
	r = iptables(str(sys.argv[1]))
else:
	r = iptables(str('/var/log/remote/iptables.log'))
a = r.read()
