import datetime
from db import *
from pprint import pprint
import json
from flask import Flask
#app = Flask(__name__)
prefix = '/'

@app.route('/')
def show_graphs(limit=5):
	l = int(limit)
	print "Limit is %d" % l
	output = {
		'activity': return_activity_plot(limit=l),
		'top-ports': return_top_ports(limit=l),
		'top-addresses': return_top_addresses(limit=l),
		'top-attacks': return_top_attacks(limit=l),
		'countries': return_top_countries(limit=l),
		'regions': return_top_regions(limit=l)
	}
		# read it out now
	return Response(json.dumps(output, sort_keys=True, indent=4),'text/json')
#app.url_map.strict_slashes = False
#db.init_app(app)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.run(host='0.0.0.0',port=8080)

