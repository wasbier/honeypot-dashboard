from db import *
from sqlalchemy import *
#func, select, insert, update, _or, _and
from random import choice
from random import randint as random
from pprint import pprint
import sys
import datetime as date
from collections import Counter
from utils import Message

#import logging
#logging.basicConfig()
#logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

drange = 60

#setting_query = "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"
setting_query = "SET sql_mode = ''"
search_query = "select log_date from log group by ((60/%d) * hour( log_date ) + floor( minute( log_date ) / %d ));" % (drange,drange)

db.engine.execute(setting_query)

result = db.engine.execute(search_query)
total_times = result.rowcount
result = result.fetchall()
pprint(result)
count = 0

lim = 20
thres = 5

while (count < total_times):
	current = result[count][0]
	try:
		next = result[count + 1][0]
	except IndexError:
		next = current + date.timedelta(minutes=int(drange))
	count += 1
	# "nmap" check... witha buncha queries pass in db,thres
	query = select([Log.source_ip.label('source'),func.count(Log.source_ip).label('count')]).where( (Log.log_date > current) & (Log.log_date < next) ).group_by(Log.source_ip).order_by('count DESC').limit(lim)
	iprange = db.session.execute(query).fetchall()
	for ipp in iprange:
		source = ipp[0]
		total = int(ipp[1])
		if total > thres:
			# now check for shit that relates whether port count is higher or not. swed
			q = select([func.count(distinct(Log.destination_port))]).where( (Log.source_ip==str(source)) & (Log.log_date > current) & (Log.log_date < next) )
			ports = db.session.execute(q).fetchone()
			unique_ports = int(ports[0])
			if unique_ports > thres:
				l = "portscan"
				print "Possible %s! from %s!" % (l,source)
				a = db.session.query(Log).filter( Log.log_date.between(current,next) & (Log.source_ip == str(source)) ).update({Log.type: l}, synchronize_session='fetch')
	# write in bulk for each date range
	db.session.commit()

	###
	## NOW CHECK DDOS ##
	###
	dthres = 50

	query = select([Log.destination_port.label('source'),func.count(Log.destination_port).label('count')]).where( (Log.log_date > current) & (Log.log_date < next) ).group_by(Log.destination_port).order_by('count DESC').limit(lim)
	portrange = db.session.execute(query).fetchall()
	for ppp in portrange:
		port = int(ppp[0])
		total = int(ppp[1])
		if total > dthres:
			# we're looking at a high port count relatively
			q = select([func.count(distinct(Log.source_ip))]).where( (Log.destination_port==str(port)) & (Log.log_date > current) & (Log.log_date < next) & (Log.type==None) )
			unique_ips = int(db.session.execute(q).fetchone()[0])
			if unique_ips > 10: # totally abitrary, but its great
				# now for some magic,
				#a = select([distinct(Log.source_ip)]).where( (Log.destination_port==str(port)) & (Log.log_date > current) & (Log.log_date < next) )
				# l = choice(['portscan','dos','bruteforce','vercheck'])
				l = "dos"
				print "Possible %s! from %s!" % (p,source)
				a = db.session.query(Log).filter( Log.log_date.between(current,next) & (Log.destination_port==str(port)) ).update({Log.type: l}, synchronize_session='fetch')
	db.session.commit()