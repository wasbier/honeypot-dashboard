from pprint import pprint

class Message():
	### Class object for messages
	messages = {
		'source_ip': None,
		'destination_ip': None,
		'source_port': None,
		'destination_port': None,
		'ttl': None,
		'packet_length': None,
		'date': None
	}
	comment = ""

	def __init__(self):
		self.messages = {
			'source_ip': None,
			'destination_ip': None,
			'source_port': None,
			'destination_port': None,
			'ttl': None,
			'packet_length': None,
			'date': None
		}
		self.comment = ""

	def __repr__(self):
		str = ""
		for key,value in self.messages.items():
			if key != "date":
				str = str + "%s = %s,\t" % (key,value)
		if self.comment is None:
			str = str + "comment = %s\t" % "Unset"
		else:
			str = str + "comment = %s\t" % self.comment

		return str

	def __getitem__(self,key):
		if key in self.messages:
			return self.messages[key]
		else:
			return None

	# i know its bad
	def __str__(self):
		return __repr__()

	def add(self,item,value):
		if item in self.messages.keys():
			self.messages[item] = value
			return True
		else:
			return False

	def setComment(self,comment):
		self.comment = comment
