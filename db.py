#from config import *
import flask
import sys
from flask import Response
from flask_sqlalchemy import SQLAlchemy
from pprint import pprint
#from marshmallow import Schema, fields

host = 'localhost'
username = '<REDACTED>'
password = '<REDACTED>'
database = 'logs' ## CHANGE THIS ##

#app = flask.Flask(__name__)
app = flask.Flask('site')

# loool
app.config['SQL_DATABASE_URI'] = 'mysql://%s:%s@%s/%s' % (username,password,host,database)
app.config['SQLALCHEMY_DATABASE_URI'] = app.config['SQL_DATABASE_URI']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
#db.init_app(app)
db = SQLAlchemy(app)
db.init_app(app)

class Log(db.Model):
	id = db.Column(db.Integer, primary_key=True) # do not use, automatic incremented, pull it whe you're writing a module to perofrm an update later. from queues.
	log_date = db.Column(db.DateTime()) # datetime formated date, required by ALL
	log_source = db.Column(db.String(128)) # source is the syslog source, if not known,
	source_ip = db.Column(db.String(128)) # source - used by ALL message types
	destination_ip = db.Column(db.String(128)) # destination, always the same, so it can be null for almost all cases!
	source_port = db.Column(db.Integer) # source port could be null esp. for auth
	destination_port = db.Column(db.Integer) # destination port, where the traffic was headed, could be null for auth
	ttl = db.Column(db.Integer) # ttl, could be null
	len = db.Column(db.Integer) # length of packet, could be null
	type = db.Column(db.String(256)) # type is null if a message hasn't been processed, once processed it is set to "ddos" "nmap" "auth-failure" "auth-escalation"
	message = db.Column(db.String(256)) # message contains the auth and details - like "allow" or "drop" and for auth "failure" and "success" , it can be null without major effects for FW rules
	is_mlbad = db.Column(db.Boolean()) # NULL = unset, machine has not analyzed message yet, False = considered "good" by machine, True is considered "Bad" by the machine
	def __init__(self,date,source_ip,destination_ip,source_port,destination_port,ttl,len,comment):
		self.log_date = date
		self.source_ip = source_ip
		self.destination_ip = destination_ip
		self.source_port = source_port
		self.destination_port = destination_port
		self.ttl = ttl
		self.len = len
		self.message = comment

class Geo(db.Model):
	#id = db.Column(db.Integer, primary_key=True)
	country_code = db.Column(db.String(128),unique=True,primary_key=True)
	country_name = db.Column(db.String(128))
	count = db.Column(db.Integer())

	def __init__(country_code,country_name):
		self.country_code = country_code
		self.country_name = country_name
		self.total = 0

if __name__ == '__main__':
	if len(sys.argv) > 1:
	        database = str(sys.argv[1])
	pprint(db.create_all())
else:
	def return_top(query,title,limit):
		q = db.session.execute(query,{'lim':limit})
		db.session.commit()
		result = []
		result.append([title,'Count'])
		for r in q:
			t = str(r[0])
			c = int(r[1])
			result.append([t,c])
		return result

	def return_top_attacks(limit=20):
		print "Running top attacks"
		change = 'Type'
		query = 'select type as attack, count(type) as count from log where type != "NONE" and type != "None" and type != "Null" group by type order by count desc limit :lim'
		return return_top(query,change,limit)

	def return_top_ports(limit=20):
		change = 'Port'
		query = 'select destination_port as dpt, count(destination_port) as count  from log  group by destination_port order by count desc limit :lim'
		return return_top(query,change,limit)

	def return_top_countries(limit=20):
		change = "Country"
		query = "select country_name,count from geo where count > 10 group by country_name order by count desc limit :lim;"
		return return_top(query,change,limit)

	def return_top_regions(limit=20):
		change = "Country"
		query = "select country_code,count from geo where count > 10 group by country_name order by count desc limit :lim;"
		return return_top(query,change,limit)

	def return_top_addresses(limit=20):
		change = "IP Addresses"
		query = 'select source_ip as source, count(source_ip) as count from log where source_ip not like "127.0%" and source_ip not like "fe80%" group by source_ip order by count desc limit :lim'
		return return_top(query,change,limit)

	def return_activity_plot(range=10,limit=20):
		query = "select log_date,count(*) as total from log group by ( hour( log_date ) + floor ( minute( log_date ) / :range )) limit :lim;"
		q = db.session.execute(query,{'range':range,'lim':limit})
		result = []
		for r in q:
			t = "%s" % str(r[0])
			c = int(r[1])
			result.append([t,c])
		return result

	def return_logs(limit=100):
		q = "select log_date as date,source_ip as address,ttl,source_port as source,destination_port as destination,type,is_mlbad as machine from log order by date limit :lim"
		r = db.session.execute(q,{'lim':limit})
        	for a in r:
			pprint(a)