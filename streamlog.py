import datetime
import socket
import dateutil
import dateutil.parser
import pycountry
import logging
import SocketServer
from utils import Message
from pprint import pprint
from utils import Message
from geoip import geolite2
from db import *

HOST = '127.0.0.1'
PORT = 5145
BUFFER_SIZE = 2048

# used stack overflow for udp socket handler

class SyslogUDPHandler(SocketServer.BaseRequestHandler):
	
	def lookup_geo(self,record):
		ip = record #['source_ip']
		match = geolite2.lookup(ip)
		if match is not None:
			unknown = False
			country_code = str(match.country)
			try:
				unknown = False
				country_name = str(pycountry.countries.lookup(country_code).name)
				print "Added Country for ip %s: %s" % (ip,country_name)
			except:
				print "Unknown Country for ip %s: %s \t %s " % (ip,country_code,country_name)
				country_name = "Unknown"
				unknown = True
			if not unknown:
				if "," in country_name:
					country_name = country_name.split(",")[0]
				query = "insert into geo(country_code,country_name,count) VALUES (:code,:name,1) on duplicate key update count = count + 1;"
				#pprint(query)
				q = db.session.execute(query,{'code':country_code,'name':country_name})
				db.session.commit()
	def process(self,line):
		final = Message()
		messages = {
			'src': 'source_ip',
			'dst': 'destination_ip',
			'spt': 'source_port',
			'dpt': 'destination_port',
			'ttl':'ttl',
			'len': 'packet_length'
		}
		od = str(line).lower().split()
		#pprint(od)
		#pprint(od[1])
		#print("\r\n\r\n")
		date = dateutil.parser.parse(str(od[1])) # datetime.datetime.strptime(str(od[1]),"%b %d %Y %H:%M:%S")
		del(od[0:8])
		# now get our data
		final.add('date',date)
		for field in od:
			of = field.split("=")
			# because sometimes its an int?????
			key = str(of[0]).split()[0]
			if key in messages.keys():
				final.add(messages[key],of[1])
		if final['destination_port'] is None:
			print "Unknown Value!"
			#pprint(final)
		else:
			l = Log(date,final['source_ip'],final['destination_ip'],final['source_port'],final['destination_port'],final['ttl'],final['len'],"fw_accept")
			self.lookup_geo(final['source_ip'])
			db.session.add(l)
			db.session.commit()
			print "Adding value to database %s (%s) -> %d" % (final['source_ip'],str(date),int(final['destination_port']))
		return final

	def handle(self):
		print("doing")
		data = bytes.decode(self.request[0].strip(), encoding="utf-8")
		self.process(data)
		socket = self.request[1]
		print( "%s : " % self.client_address[0], str(data))
		od = str(data).lower().split()
		#pprint(data)
if __name__ == "__main__":
	try:
		print("running")
		server = SocketServer.UDPServer((HOST,PORT), SyslogUDPHandler)
		server.serve_forever(poll_interval=0.5)
	except (IOError, SystemExit):
		print("error")
		raise
	except KeyboardInterrupt:
		print ("Crtl+C Pressed. Shutting down.")

